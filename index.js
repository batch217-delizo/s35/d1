const express = require('express');
const app = express();
const mongoose = require('mongoose');
const port = 3001;

//Section MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.qfnw1x1.mongodb.net/B217_to-do?retryWrites=true&w=majority")

const taskSchema = new mongoose.Schema({
    name: {
      type: String,
      required: [true, "Task name is required"]
    },
    status: {
      type: String,
      default: "pending"
    }
})

const Task = mongoose.model('Task', taskSchema)




app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.post("/task", (req, res) => {
    Task.findOne({name: req.body.name}, (err, result) => {
      if(result != null && result.name == req.body.name) {
        return res.send("Duplicate found");
      } else {
        let newTask = new Task({
            name: req.body.name
        })

        newTask.save((saveErr, saveTask)=> {
            if(saveErr) {
              return console.log(saveErr)
            } else {
              return res. status(201).send('New task created')
            }
        })
      }
    })
})



const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "username name is required"]
  },

  password: {
    type: String,
    required: [true, "password name is required"]
  }
})

const User = mongoose.model('User', userSchema)

app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {
      if(result != null && result.username == req.body.username ) {
        return res.send("Duplicate User found");
      } else if(req.body.username == ""){
        return res.send("username is required")
      } else if(req.body.password == ""){
        return res.send("password is required")
      }else {
        let newUser = new User({
            username: req.body.username,
            password: req.body.password
        })

        newUser.save((saveErr, saveTask)=> {
            if(saveErr) {
              return console.log(saveErr)
            } else {
              return res.status(201).send('New User registered')
            }
        })
      }
    })
})


// Update to async and await
app.post("/register", async (req, res) => {
    let result =  await User.findOne({username: req.body.username, password: req.body.password})

    try {

      if(result != null && result.username == req.body.username) {
        return res.send("Duplicate User found");
      }else if(req.body.username == ""){
        return res.send("username is required")
      } else if(req.body.password == ""){
        return res.send("password is required")
      }else {
        let newUser = new User({
            username: req.body.username,
            password: req.body.password
        })

        newUser.save((saveErr, saveTask)=> {
            if(saveErr) {
              return console.log(saveErr)
            } else {
              return res.status(201).send('New User registered /register')
            }
        })
      }

    } catch(error) {
      res.send(error)
    }
})

app.get("/readUsers",async (req, res) => {
   let users = await User.find({});
   try {
     res.send(users);
    } catch(error) {
     res.send(error) 
    }
});


app.listen(port, () => {
  console.log(`Server is running at port: ${port}`);
})

